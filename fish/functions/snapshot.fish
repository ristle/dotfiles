function snapshot --wraps=sudo\ btrfs\ subvolume\ snapshot\ /\ /.snapshots/\(date\ \|tr\ \'\ \'\ \'_\'\) --description alias\ snapshot=sudo\ btrfs\ subvolume\ snapshot\ /\ /.snapshots/\(date\ \|tr\ \'\ \'\ \'_\'\)
  sudo btrfs subvolume snapshot / /.snapshots/(date |tr ' ' '_') $argv; 
end
