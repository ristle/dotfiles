function dockerC --description 'Docker snippet for courier launch'
    set -l options h/help m/mode 'd/docker=' 'f/folder='

    argparse $options -- $argv
    if set --query _flag_help
        printf "Fucking help\n"
        printf "Try to run folders in docker\n"
        printf "   - h/help\n"
        printf "   - d/docker - docker name\n"
        printf "   - f/folder - folder which we want push in docker\n"
        printf "   - m/mode   - mode docker run or go2docker"
        return
    end

    set --query _flag_docker; or set -l _flag_docker sda-ubuntu-20.04-ristle-942bee4
    set --query _flag_folder; or set -l _flag_folder ~/Ros/gleb
    set --query _flag_mode; or set -l _flag_mode true
    if test _flag_mode
        podman run -it --rm --privileged --net host \
            -v /dev/snd:/dev/snd \
            -v $HOME/.Xauthority:/tmp/.Xauthority \
            -v /tmp/.X11-unix:/tmp/.X11-unix \
            -v $_flag_folder:/usr/courier/src \
            -v $PWD:/app \
            -v /dev/bus/usb:/dev/bus/usb \
            -e DISPLAY=unix$DISPLAY \
            -e XAUTHORITY=/tmp/.Xauthority \
            $_flag_docker \
            bash
    else
        podman exec -it $_flag_docker bash
    end
end
