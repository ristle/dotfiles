function rmi --wraps=rm-improved --description 'alias rmi=rm-improved'
  rm-improved $argv
        
end
