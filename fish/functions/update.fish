function update --wraps='sudo pacman -Sy' --description 'alias update=sudo pacman -Sy'
  sudo pacman -Sy $argv; 
end
