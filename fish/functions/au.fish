function au
    if test (screen -list |grep 'update' |awk '{print $1}' |wc -l) -gt 0
        screen -R (screen -list |grep 'update' |awk '{print $1}')
    else
        set_color red
        echo "No active session!"
        set_color normal
    end
end
