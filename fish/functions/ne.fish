function ne
    if set --query no_execute
        set_color blue
        echo "Erase no_execute flag"
        set_color normal
        set --erase no_execute
        set --erase last_env
        set --erase current_path
        for name in (echo $PWD | tr '/' '\n' )                                                  
          set -g current_path (echo $current_path"/"$name)
          if test -e $current_path/.env.fish
              set -g last_env $current_path"/".env.fish
          end
        end
        if not test -z $last_env
          set_color green
          echo "Proccesing..."
          echo "----------------"
          echo
          set_color blue
          cat $last_env
          set_color green
          echo
          echo "----------------"
          set_color normal
          
          source $last_env
        end 
    else
        set_color blue
        echo "Setting up no_execute flag"
        set_color normal
        export no_execute=1
        cd
        cd -
    end
end
