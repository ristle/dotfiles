function r --wraps='projectdo run' --description 'alias r=projectdo run'
  projectdo run $argv
        
end
