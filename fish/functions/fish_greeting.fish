function fish_greeting
    #pfetch
    if test (which fm6000)
        fm6000 -r -n -c random
    else if test (which pfetch)
        pfetch
    else
        set_color red
        printf 'Plz try to install'
        set_color blue
        printf 'pfetch|fm6000'
        set_color normal
    end
    if not test (which exa)
        printf 'No '
        set_color red
        printf 'exa '
        set_color normal
        printf 'is installed. Try to built it manually!\n'
    end

    #set_color blue
    #todo.sh list
    #set_color normal
    #set_color green
    #echo "Do not forget to do mltype!"
    #set_color normal
end
