function push_gotify
set -l options h/help 't/title=' 'm/message='

argparse $options -- $argv

if set --query _flag_help
echo "Type --title or -t to set title of the notify message"
echo "Type --message or -m to set message to notify"
return 0
end

if test -z $_flag_title || test -z  $_flag_message 
set_color red
echo "Please set title and message!"
return 1
set_color normal
end

curl $GOTIFY_SERVER"/message?token="$GOTIFY_TOKEN -F "title="$_flag_title -F "message="$_flag_message -F "priority=5"
end
