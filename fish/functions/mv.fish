function mv --wraps='advmv -gR' --description 'alias mv=advmv -gR'
  if test -e /usr/bin/advmv
    advmv -gi $argv; 
  else 
    /usr/bin/mv -i $argv
  end
end
