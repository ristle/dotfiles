function generate_rust_queries
set -l options 'f/file='

argparse $options -- $argv

if not set --query _flag_file
set_color red
echo "Please set file to work"
return 1;
end

set -l file (echo $_flag_file)
set -l name (string sub -e -2 (bat -p $file | grep 'crate::schema' | head -n 1 | awk -F '[::]' '{print $5}'))
set -l SimpleName ( bat -p $file | grep struct | head -n 1 | awk '{print $3}')
set -l NewStruct (bat -p $file | grep struct | tail -n 1 | awk '{print $3}' | awk -F '[<]' '{print $1}')
set -l values_count (math -s 0 (bat -p $file |grep ': ' | wc -l ) / 2)
printf "\n\npub fn insert_$name(conn: &mut PgConnection"
for value in (bat -p $file | grep ': ' | tail -n $values_count)
printf ", "(string replace ',' '' (string replace "&'a str" '&str' (string replace  'pub' '' $value)))
end

printf ") {\nuse crate::schema::$name;\n\n"
printf "let new_$name = $NewStruct {"
set  znak 0 
for value in (bat -p $file | grep ': ' | tail -n $values_count)
if test $znak -gt 0
printf ", " 
end
set znak (math $znak + 1)
printf (string replace ':' '' ( echo $value | awk '{print $2}' ))
end
printf " };"
printf "\n\ndiesel::insert_into(posts::$name)
        .values(&new_$name)
        .returning($SimpleName::as_returning())
        .get_result(conn)
        .expect(\"Error saving new $name\")
}"
end
