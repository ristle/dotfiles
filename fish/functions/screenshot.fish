function screenshot
    set -l options h/help s/selection "n/name="


    argparse $options -- $argv

    if set --query $_flag_help
        set_color green

        echo "Command to help in creation of screenshot"
        echo " -h/help      -- help"
        echo " -s/selection -- Do selection orwhiole screen"
        echo " -n/name      -- name of output file"
    end


    set --query _flag_name; or set -l _flag_name (echo (date)".png")
    set -l folder ~/Pictures/Screenshots

    sleep 1
    if set --query _flag_selection
        scrot -s $folder$_flag_name 
    else
        scrot $folder$_flag_name 
    end
    xclip -selection clipboard -target image/png -i $folder$_flag_name

    dunstify "Screentshot with name \"$folder$_flag_name\" saved and put in clipboard"
end
