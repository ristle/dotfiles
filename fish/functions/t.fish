function t --wraps='projectdo test' --description 'alias t=projectdo test'
  projectdo test $argv
        
end
