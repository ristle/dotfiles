function create_git_template
    set -l options 't/type=' 'n/name=' 'p/template_path=' h/help

    argparse $options -- $argv

    set --query _flag_template_path; or set -l _flag_template_path ~/.templates/gitlab

    if not set --query _flag_type
        set_color red
        echo "Insert template type name"
        set_color normal
        return 1
    end

    if not set --query _flag_name
        set_color red
        echo "Please type name on the projects to create"
        set_color normal
        return 1
    end

    if not test -e $_flag_template_path'/'$_flag_type
        set_color red
        printf "Choose Properly ttemplate folder in "
        set_color green
        printf $_flag_template_path
        set_color red
        printrf " directory"
        return 1
    end

    git init --template=$_flag_template_path'/'$_flag_type

    for file_name in (/usr/bin/ls $_flag_template_path'/'$_flag_type)
        mv -i .git/$file_name ./
        sed -i 's/project_name/'$_flag_name'/g' $file_name
    end

end
