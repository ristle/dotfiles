function btp --description 'Builld project using cmake inplace'
    set -l options h/help r/rebuild 'f/folder=' 't/type='
    argparse $options -- $argv
    if set --query _flag_help
        printf "Fucking help\n"
        printf "Try to build speciifc programm using cmake in place\n"
        printf "   - h/help\n"
        printf "   - f/folder\n"
        return 0
    end
    set --query _flag_folder; or set -l _flag_folder build
    set --query _flag_type; or set -l _flag_type Release
    # set --query _flag_rebuild; or set -l _flag_rebuild 0
    if set --query _flag_rebuild
        if test -d $_flag_folder
            if test -G $_flag_folder
                rm -rf $_flag_folder
            else
                sudo rm -rf $_flag_folder
            end
        end
    end
    cmake -B $_flag_folder
    cmake --build $_flag_folder --config $_flag_type -j12
    if test $status
        sudo cmake --install $_flag_folder --strip
    end
end
