function full_upgrade --wraps='paru -Syu --noconfirm --sudoloop' --description 'alias full_upgrade=paru -Syu --noconfirm --sudoloop'
  paru -Syu --noconfirm --sudoloop $argv; read
end
