function generate_diesel_struct
set -l options 'f/file=' 

argparse $options -- $argv

if not set --query _flag_file 
set_color red
echo "Please set --file of SQL"
return 1
end

set -l file (echo $_flag_file)

set -l table_name (bat -p $file |grep table | awk '{print $3}')
echo "use diesel::prelude::*;"
echo ""
echo "#[derive(Insertable)]"
#echo "#[derive(Queryable, Selectable)]"
echo "#[diesel(table_name = crate::schema::$table_name)]"
#echo "#[diesel(check_for_backend(diesel::pg::Pg))]"
echo "pub struct New$table_name<'a> {"

for full_name in (bat -p $file | grep '    ')
set -l name (echo $full_name | awk '{print $1}')
set -l type (echo $full_name | awk '{print $2}')
set -l final_type (echo $type)
 
if test $name = "id"
continue;
end
switch $type
case "varchar"
set final_type "&'a str"
case "int"
set final_type "i32"
case "float"
set final_type "f32"
case "boolean"
set final_type "bool"
case "date"
set final_type "Date"
case "serial"
continue
case '*'
# echo "Type: $type"
#return 1
end
printf "\tpub $name: $final_type,\n"


end
echo "}"
end
