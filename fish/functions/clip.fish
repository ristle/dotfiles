function clip --description 'to clipboard'
    if test (count $argv) -ge 1
        set_color green
        printf "Coping "
        set_color blue
        printf $argv" \n"
        xclip -selection clipboard $argv[1]
    else
        set_color red
        echo "No input is provided"
        set_color normal
    end
end
