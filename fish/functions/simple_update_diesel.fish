function simple_update_diesel
set -l options 'l/lower_name=' 's/struct_name=' 'p/param=' 't/param_type=' 'e/equal='

argparse $options -- $argv

echo "

pub fn update_"$_flag_param"_"$_flag_lower_name"(
    conn: &mut PgConnection,
    id: i32,
    _"$_flag_param": "$_flag_param_type",
) -> std::result<"$_flag_struct_name", diesel::result::Error> {
    use crate::db::schema::$_flag_lower_name::dsl::{$_flag_lower_name, $_flag_param};

    diesel::update($_flag_lower_name)
        .find(id)
        .set($_flag_param.eq(_$_flag_param))
        .returning($_flag_struct_name::as_returning())
        .get_result(conn)?
}


"

end
