function connect_displays
    set -l count_of_displays (xrandr -q |grep -w connected | wc -l)

    if test $count_of_displays -eq 1
        return 0
    end

    set -l main_display_name (xrandr -q | grep -w connected | head -n 1 | awk '{print $1}')
    set -l number_of_display 0

    for display in (xrandr -q | grep -w connected| tail -n (math $count_of_displays - 1))
        set -l display_name (echo $display | awk '{print $1}')
        if test $number_of_display -eq 0
            xrandr --output $display_name --right-of $main_display_name
        else
            xrandr --output $display_name --left-of $main_display_name
        end
        set number_of_display (math $number_of_display + 1)
    end
end
