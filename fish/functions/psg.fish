function psg --wraps=ps\ -ef\ \\\|\ grep --description alias\ psg=ps\ -ef\ \\\|\ grep
  ps -ef \| grep $argv; 
end
