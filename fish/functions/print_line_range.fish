function print_line_range
    set -l options h/help 'f/file=' 't/time=' 'r/range=' c/use_cat

    argparse $options -- $argv
    if set --query _flag_help
        set_color blue
        echo "- f/file    = file location to ros log"
        echo "- r/range   = log range. Example -r 50. By default, range is equal 5o"
        echo "- t/time    = specific time to find from start in seconds!"
        echo "- c/use_cat = use cat or bat by default"
        return 0
    end

    if not set --query _flag_file && not set --query _flag_time
        set_color red
        echo "Specify file and time to cat!"
        set_color normal
        return 1
    end

    set --query _flag_range; or set -l _flag_range 50

    set -l found_line (get_line_from_log -f $_flag_file -t $_flag_time)
    echo "Found: \n\t"
    echo $found_line
    set -l needed_line (cat -n $_flag_file |grep (string sub -s 2 -e -1 (echo $found_line |awk '{print $2}')) | awk '{print $1}')

    set -l start_range 0
    set -l end_range (math $needed_line + $_flag_range)

    if test (math $needed_line - $_flag_range) -gt 0
        set start_range (math $needed_line - $_flag_range)
    end

    set -l range (echo  $start_range":"$end_range)
    echo "Range: "$range

    if set --query _flag_use_cat
        cat -n $_flag_file | sed $start_range","$end_range"!d"
    else
        bat $_flag_file -r $range
    end
end
