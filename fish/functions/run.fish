function run
    set -l options h/help 'p/programm=' 'f/folder='

    argparse $options -- $argv
    if set --query _flag_help

        printf "Fucking help\n"
        printf "Try to run speciifc programm on background using screen\n"
        printf "   - h/help\n"
        printf "   - p/programm\n"
        printf "   - f/folder\n"
        return 0
    end

    set --query _flag_programm; or set -l _flag_programm codium
    set --query _flag_folder; or set -l _flag_folder .

    printf "Starting "$_flag_programm

    screen -d -m $_flag_programm $_flag_folder
end
