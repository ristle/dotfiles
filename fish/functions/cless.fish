function cless --wraps='ccat |less' --description 'alias cless=ccat |less'
    ccat $argv | less -R
end
