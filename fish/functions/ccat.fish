function ccat --wraps='pygmentize -g ~/.programming/SDA/c' --description 'alias ccat=pygmentize -g ~/.programming/SDA/c'
    pygmentize -g $argv
end
