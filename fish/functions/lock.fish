function lock
set -l PICTURE /tmp/i3lock.png
set -l SCREENSHOT "scrot $PICTURE"

set -l BLUR "5x4"

scrot $PICTURE

convert $PICTURE -blur $BLUR $PICTURE
i3lock -i $PICTURE
rm $PICTURE
end
