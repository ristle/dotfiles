function __bobthefish_pretty_ms --description 'Millisecond formatting for humans' --no-scope-shadowing --argument ms interval
                            set -l interval_ms
                            set -l scale 1

                            switch $interval
                                case s
                                    set interval_ms 1000
                                case m
                                    set interval_ms 60000
                                case h
                                    set interval_ms 3600000
                                    set scale 2
                            end

                            switch $FISH_VERSION
                                case 2.0.\* 2.1.\* 2.2.\* 2.3.\*
                                    # Fish 2.3 and lower doesn't know about the -s argument to math.
                                    math "scale=$scale;$ms/$interval_ms" | string replace -r '\\.?0*$' $interval
                                case 2.\*
                                    # Fish 2.x always returned a float when given the -s argument.
                                    math -s$scale "$ms/$interval_ms" | string replace -r '\\.?0*$' $interval
                                case \*
                                    math -s$scale "$ms/$interval_ms"
                                    echo -ns $interval
                            end
                        
end
