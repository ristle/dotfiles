function vpn
    set -l options s/status

    argparse $options -- $argv

    if set --query _flag_status
        if test (ifconfig |grep stas| wc -l) -gt 0
            echo "Vpn is Up"
        else
            echo "Vpn is Down"
        end
        return 0
    end

    if test (ifconfig |grep stas |wc -l) -gt 0
        sudo wg-quick down ~/Documents/stas.conf 2>/tmp/wg-(date +%s)
        set_color green
        echo "Vpn is Down"
        set_color normal
    else
        sudo wg-quick up ~/Documents/stas.conf 2>/tmp/wg-(date +%s)
        set_color green
        echo "Vpn is Up"
        set_color normal
    end
end
